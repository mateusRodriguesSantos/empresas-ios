//
//  Enterprise.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import Foundation

class Enterprises: Codable{
    var enterprises:[Enterprise]
}

class Enterprise: Codable {
    var id: Int?
    var email_enterprise:String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var own_enterprise: Bool?
    var enterprise_name: String?
    var photo: String?
    var description: String?
    var city: String?
    var country: String?
    var value: Int?
    var share_price: Double?
    var enterprise_type:EnterpriseType?
}

class EnterpriseType: Codable {
    var id: Int?
    var enterprise_type_name: String?
}
