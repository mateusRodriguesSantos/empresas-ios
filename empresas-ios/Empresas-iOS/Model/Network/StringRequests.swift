//
//  StringRequestions.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import Foundation

struct StringRequests {
    static var login = "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in"
    static var searchEnterprises = "https://empresas.ioasys.com.br/api/v1/enterprises"
    static var image = "https://empresas.ioasys.com.br"
}
