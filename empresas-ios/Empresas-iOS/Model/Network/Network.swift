//
//  Network.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import Foundation

enum Actions {
    case login
    case search
}

enum Status {
    case success
    case error
    case conectionFail
}

protocol ObserverRequest {
    /**
     Notify the end of request for the class that call the peformRequest method
     - Authors: Mateus R.
     - Returns: nothing
     - Parameter array: Enterprises
     - Parameter statusRequest: error or succes
     */
    func notifyEndRequest(_ array:[Enterprise],_ statusRequest: Status)
}

class Network:ObserverRequest {

    ///Call the notifyEndRequest method os each observers in allObservers list
    func notifyEndRequest(_ array: [Enterprise],_ statusRequest: Status) {
        for observer in allObservers {
            observer.notifyEndRequest(array, statusRequest)
        }
    }
    
    ///All observers of this class
    var allObservers:[ObserverRequest] = []
    
  
    
    
}

//MARK: Request
extension Network{
    /**
     Create a request and call method to parse for login
     - Authors: Mateus R.
     - Returns: nothing
     - Parameter email: String
     - Parameter senha: String
     */
    func peformRequestLogin(email:String,senha:String){
        
        var urlRequest:URLRequest?
        
        guard let URL = URL(string: StringRequests.login) else{
            return
        }
        urlRequest =  URLRequest(url: URL)
        
        let parameters = "{\n  \"email\" : \"\(email)\",\n  \"password\" : \"\(senha)\"\n}"
        
        let postData = parameters.data(using: .utf8)
        
        
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        urlRequest?.httpMethod = "POST"
        urlRequest?.httpBody = postData
        
        let session = URLSession(configuration: .default)
        
        guard let request = urlRequest else { return  }
        
        let task = session.dataTask(with: request) { (data, response, error)  in
            if error != nil{
                self.notifyEndRequest([], .conectionFail)
                print("Error in Request")
            }
            
            if data?.isEmpty == false{
                if let dataForParse = data{
                    guard let resp = response else { return  }
                    self.manageResponse(.login,data: dataForParse, response: resp)
                }
            }
        }
        task.resume()
    }
    
    /**
     Create a request and call method to parse for search enterprises
     - Authors: Mateus R.
     - Returns: nothing
     - Parameter accessToken: String
     - Parameter client: String
     - Parameter uid: String
     */
    func peformRequestSearch(accessToken:String,client:String,uid:String){
        
        var urlRequest:URLRequest?
        
        guard let URL = URL(string: StringRequests.searchEnterprises) else{
            return
        }
        urlRequest =  URLRequest(url: URL)
        
        
        urlRequest?.addValue(accessToken, forHTTPHeaderField: "access-token")
        urlRequest?.addValue(client, forHTTPHeaderField: "client")
        urlRequest?.addValue(uid, forHTTPHeaderField: "uid")
        
        
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        urlRequest?.httpMethod = "GET"
        
        let session = URLSession(configuration: .default)
        
        guard let request = urlRequest else { return  }
        
        let task = session.dataTask(with: request) { (data, response, error)  in
            if error != nil{
                self.notifyEndRequest([], .conectionFail)
                print("Error in Request")
            }
            
            if data?.isEmpty == false{
                if let dataForParse = data{
                    guard let resp = response else { return  }
                    self.manageResponse(.search,data: dataForParse, response: resp)
                }
            }
        }
        task.resume()
    }
    
}

//MARK: Parse
extension Network{
      /**
       Method for call the parse methods
       - Authors: Mateus R.
       - Returns: nothing
       - Parameter action: Actions
       - Parameter data: Data for parse
       - Parameter response: URLResponse
       */
      func manageResponse(_ action:Actions,data:Data,response:URLResponse) {
          if action == .login{
              manageLogin(data: data, response: response)
          }else if action == .search{
              manageSearch(data: data)
          }
      }
      
      /**
       Method for parse login data
       - Authors: Mateus R.
       - Returns: nothing
       - Parameter data: Data for parse
       - Parameter response: URLResponse
       */
      func manageLogin(data:Data,response:URLResponse){
          do {
              let decoder = JSONDecoder()
              let decode = try decoder.decode(Headers.self, from: data)
              
              if decode.success{
                  //get access token, uid an client
                  
                  guard let responseParse = response as? HTTPURLResponse else{
                      return
                  }
                  
                  if responseParse.statusCode == 200{
                      
                      guard let headers = responseParse.allHeaderFields as? [String:String] else {
                          return
                      }
                      
                      let accessToken = headers["access-token"]
                      let client = headers["client"]
                      let uid = headers["uid"]
                      
                      
                      UserDefaults.standard.setValue(accessToken ?? "nothing", forKey: "accessToken")
                      UserDefaults.standard.setValue(client ?? "nothing", forKey: "client")
                      UserDefaults.standard.setValue(uid ?? "nothing", forKey: "uid")
                      
                      notifyEndRequest([],.success)
                      
                      print("AccessToken: \(accessToken ?? "nothing") / client: \(client  ?? "nothing") / uid: \(uid ?? "nothing")")
                  }
              }else{
                  notifyEndRequest([],.error)
                  return
              }
              
              print("Result of request \(decode.success)")
              
          } catch {
              print(error.localizedDescription)
          }
      }
      
      /**
       Method for parse search data
       - Authors: Mateus R.
       - Returns: nothing
       - Parameter data: Data for parse
       */
      func manageSearch(data:Data){
          print(data)
          do {
              let decoder = JSONDecoder()
              let decode = try decoder.decode(Enterprises.self, from: data)
              
              print(decode.enterprises)
              notifyEndRequest(decode.enterprises,.success)
          } catch {
              notifyEndRequest([],.error)
              print(error.localizedDescription)
          }
      }
}
