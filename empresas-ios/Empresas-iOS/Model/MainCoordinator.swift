//
//  MainCoordinator.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import Foundation

import UIKit

class MainCoordinator: Coordinator {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        guard let stringParse = UserDefaults.standard.value(forKey: "accessToken") as? String else{
            
            let loginVC = LoginViewController()
            loginVC.coordinator = self
            
            navigationController.pushViewController(loginVC, animated: true)
            
            return
            
        }
        
        if UserDefaults.standard.value(forKey: "accessToken") == nil || stringParse == "nothing"{
            
            
            let loginVC = LoginViewController()
            loginVC.coordinator = self
            
            navigationController.pushViewController(loginVC, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            
            guard let homeVC = storyboard.instantiateInitialViewController() as? HomeViewController else{
                return
            }
            
            homeVC.coordinator = self
            self.navigationController.pushViewController(homeVC, animated: true)
        }

    }
}

//MARK: Navigation Functions
extension MainCoordinator{
    /**
     Navigate to login view
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    func navigateToLogin(){
        let loginVC = LoginViewController()
        loginVC.coordinator = self
        
        navigationController.pushViewController(loginVC, animated: true)
    }
    
    /**
     Navigate to Home view
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    func navigateToHome(){
        DispatchQueue.main.async { [weak self] in
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            
            guard let homeVC = storyboard.instantiateInitialViewController() as? HomeViewController else{
                return
            }
            
            homeVC.coordinator = self
            self?.navigationController.pushViewController(homeVC, animated: true)
        }
    }
    
    /**
     Navigate to Detail view
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    func navigateToDetail(_ enterprise:Enterprise)  {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        
        guard let detailVC = storyboard.instantiateInitialViewController() as? DetailViewController else{
            return
        }
        
        detailVC.coordinator = self
        detailVC.enterprise = enterprise
        self.navigationController.pushViewController(detailVC, animated: true)
    }
    
}

