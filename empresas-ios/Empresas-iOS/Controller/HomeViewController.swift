//
//  HomeViewController.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import UIKit

class HomeViewController: UIViewController {
    ///Coordinator object
    weak var coordinator:MainCoordinator?
    
    //Outlets
    @IBOutlet var baseView: HomeView!
    
    ///Filtered enterprises by searchBar
    var filteredEnterprises: [Enterprise] = []
    
    ///All Enterprises
    var enterprises:[Enterprise] = []
    
    ///Spinner for loading animation
    let child = SpinnerViewController()
    
    ///Status of searchBar
    var isSearchBarEmpty: Bool {
        return baseView.searchBar.text?.isEmpty ?? true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set up delegates and data sources
        baseView.delegate = self
        baseView?.tableView.delegate = self
        baseView?.tableView.dataSource = self
        baseView.searchBar.delegate = self
        
    }
}

extension HomeViewController:homeViewDelegate{
    func conectionFail() {
        DispatchQueue.main.async { 
            let alert = UIAlertController(title: "Alerta", message: "Falha na conexão. Certifique-se de estar conectado à internet!!!", preferredStyle: .alert)
            
            
            let action1 = UIAlertAction(title: "OK", style: .destructive) { (_) in
                
                DispatchQueue.main.async { [weak self] in
                    self?.child.willMove(toParent: nil)
                    self?.child.view.removeFromSuperview()
                    self?.child.removeFromParent()
                }
                
            }
            
            alert.addAction(action1)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func errorSearch() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alerta", message: "Sua Sessão expirou! Deseja fazer novamento o login?", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Fazer login", style: .default) { [weak self] (_) in
                self?.coordinator?.navigateToLogin()
                
                UserDefaults.standard.setValue("nothing", forKey: "accessToken")
                UserDefaults.standard.setValue("nothing", forKey: "client")
                UserDefaults.standard.setValue("nothing", forKey: "uid")
            }
            
            let action2 = UIAlertAction(title: "Não", style: .destructive) { (_) in
                
                UserDefaults.standard.setValue("nothing", forKey: "accessToken")
                UserDefaults.standard.setValue("nothing", forKey: "client")
                UserDefaults.standard.setValue("nothing", forKey: "uid")
            }
            
            alert.addAction(action1)
            alert.addAction(action2)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func activeSpinner() {
        createSpinnerView()
    }
    
    func successSearch(_ array: [Enterprise]) {
        DispatchQueue.main.async { [weak self] in
            self?.enterprises = array
            self?.baseView?.tableView.reloadData()
        }
        
        DispatchQueue.main.async { [weak self] in
            self?.child.willMove(toParent: nil)
            self?.child.view.removeFromSuperview()
            self?.child.removeFromParent()
        }
    }
    
    func createSpinnerView() {
        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !isSearchBarEmpty {
            return filteredEnterprises.count
        }
        
        return enterprises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "tableEnterprise", for: indexPath) as? EnterpriseTableViewCell else{
            return UITableViewCell()
        }
        
        var enterprise:Enterprise
        
        if !isSearchBarEmpty {
            enterprise = filteredEnterprises[indexPath.row]
        } else {
            enterprise = enterprises[indexPath.row]
        }
        
        cell.enterprise = enterprise
        cell.nomeEmpresa.text = enterprise.enterprise_name
        
        guard let stringPhoto = enterprise.photo else { return  cell}
        
        let urlString = "\(StringRequests.image)\(stringPhoto)"
        guard let URL = URL(string: urlString) else { return cell}
        
        cell.backImage.kf.setImage(with: URL)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? EnterpriseTableViewCell else { return}
        
        guard let enterprise = cell.enterprise else { return  }
        
        coordinator?.navigateToDetail(enterprise)
    }
}

extension HomeViewController : UISearchBarDelegate  {
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String){
        guard let text = searchBar.text else { return  }
        
        searchBar.showsCancelButton = true
        
        baseView.topSearchConstraint?.constant = 30
        
        UIView.animate(withDuration: 0.8) {
            self.view.layoutIfNeeded()
        }
        filterContentForSearchText(text)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        
        baseView.topSearchConstraint?.constant = 30
        
        UIView.animate(withDuration: 0.8) {
            self.view.layoutIfNeeded()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        baseView.topSearchConstraint?.constant = 120
        
        UIView.animate(withDuration: 0.8) {
            self.view.layoutIfNeeded()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        baseView.topSearchConstraint?.constant = 120
        
        searchBar.showsCancelButton = false
        
        searchBar.searchTextField.resignFirstResponder()
        
        UIView.animate(withDuration: 0.8) {
            self.view.layoutIfNeeded()
        }
    }
}

//MARK: Filtered Functions
extension HomeViewController{
    func filterContentForSearchText(_ searchText: String) {
        
        filteredEnterprises = enterprises.filter { (enterprise: Enterprise) -> Bool in
            
            guard let name = enterprise.enterprise_name else{return false}
            
            return name.lowercased().contains(searchText.lowercased())
        }
        
        baseView.tableView.reloadData()
    }
}
