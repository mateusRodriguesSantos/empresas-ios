//
//  DetailViewController.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 20/12/20.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {
    ///Coordinator object
    weak var coordinator:MainCoordinator?
    
    //Outlets
    @IBOutlet var baseView: DetailView!
    
    ///Enterprise object
    var enterprise:Enterprise?
    ///Network method
    let net = Network()
    
    ///Enterprise object
    let child = SpinnerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        baseView.nameEnterprise.text = self.enterprise?.enterprise_name
        baseView.DescriptionEnterprise.text = self.enterprise?.description
        
        net.allObservers.append(self)
        
        guard let stringPhoto = enterprise?.photo else { return  }
        
        let urlString = "\(StringRequests.image)\(stringPhoto)"
        guard let URL = URL(string: urlString) else { return}
        baseView.enterpriseImage.kf.setImage(with: URL)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        coordinator?.navigateToHome()
    }
}

extension DetailViewController{
    func createSpinnerView() {
        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
}

extension DetailViewController:ObserverRequest{
    func notifyEndRequest(_ array: [Enterprise], _ statusLogin: Status) {

    }
}
