//
//  LoginViewController.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import UIKit

class LoginViewController: UIViewController {
    ///Coordinator object
    weak var coordinator:MainCoordinator?

    ///Base View or contentView
    var baseView:LoginView?
    
    ///Spinner for animation loading
    let child = SpinnerViewController()
    
    override func loadView() {
        super.loadView()
        
        //Change view from .xib
        baseView = LoginView(frame: UIScreen.main.bounds)
        baseView?.delegate = self
        self.view = baseView
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension LoginViewController:LoginViewDelegate{
    func conectionFail() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alerta", message: "Falha na conexão. Certifique-se de estar conectado à internet!!!", preferredStyle: .alert)
            
            
            let action1 = UIAlertAction(title: "OK", style: .destructive) { (_) in
                
                DispatchQueue.main.async { [weak self] in
                    self?.child.willMove(toParent: nil)
                    self?.child.view.removeFromSuperview()
                    self?.child.removeFromParent()
                }
                
            }
            
            alert.addAction(action1)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func errorLogin() {
        // then remove the spinner view controller
        DispatchQueue.main.async { [weak self] in
            self?.child.willMove(toParent: nil)
            self?.child.view.removeFromSuperview()
            self?.child.removeFromParent()
        }
    }
    
    func activeSpinner() {
        createSpinnerView()
    }
    
    func successLogin() {
        coordinator?.navigateToHome()
        // then remove the spinner view controller
        DispatchQueue.main.async { [weak self] in
            self?.child.willMove(toParent: nil)
            self?.child.view.removeFromSuperview()
            self?.child.removeFromParent()
        }

    }

    func createSpinnerView() {
        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
}
