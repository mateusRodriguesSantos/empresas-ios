//
//  LoginView.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import UIKit

protocol LoginViewDelegate: class {
    /**
     Notify the succes of a login
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    func successLogin()
    /**
     Notify the delegate about the success of login
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    func activeSpinner()
    
    /**
     Notify the delegate about the error of login
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    func errorLogin()
    
    /**
     Notify the delegate about the fail in conection
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    func conectionFail()
}

class LoginView: UIView {
    ///Outlets
    @IBOutlet weak var LabelError: UILabel!
    @IBOutlet weak var senhaLabel: UITextField!
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet var ContentView: UIView!
    @IBOutlet weak var entrar: UIButton!
    
    ///Delegate
    weak var delegate:LoginViewDelegate?
    
    ///Network object
    let net = Network()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }
    
    override func didMoveToWindow() {
        senhaLabel.delegate = self
        emailLabel.delegate = self
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     Init the contentView from .xib
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    private func commomInit(){
        Bundle.main.loadNibNamed("Login", owner: self, options: nil)
        self.addSubview(ContentView)
        ContentView.frame = self.bounds
        ContentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
    ///Action of entrar button
    @IBAction func actionEntrar(_ sender: Any) {
        net.allObservers.append(self)
        net.peformRequestLogin(email: emailLabel.text ?? "nothing", senha: senhaLabel.text ?? "nothing")
        delegate?.activeSpinner()
    }
}

extension LoginView:ObserverRequest{
    func notifyEndRequest(_ array:[Enterprise],_ statusRequest: Status) {
        net.allObservers.removeAll()
        if statusRequest == .success{
            //Call success login
            delegate?.successLogin()
        }else if statusRequest == .error{
            //Error mensage
            DispatchQueue.main.async { [weak self] in
                
                self?.senhaLabel.layer.cornerRadius = 5
                self?.senhaLabel.layer.borderWidth = 3
                self?.senhaLabel.layer.borderColor = UIColor.red.cgColor
                
                self?.emailLabel.layer.cornerRadius = 5
                self?.emailLabel.layer.borderWidth = 3
                self?.emailLabel.layer.borderColor = UIColor.red.cgColor
                
                self?.LabelError.isHidden = false
                
                self?.delegate?.errorLogin()
            }
        }else{
            delegate?.conectionFail()
        }
    }
}

extension LoginView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async { [weak self] in
            self?.senhaLabel.layer.borderColor = UIColor.clear.cgColor
            self?.emailLabel.layer.borderColor =  UIColor.clear.cgColor
            self?.LabelError.isHidden = true
        }
    }
}

