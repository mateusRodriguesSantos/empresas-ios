//
//  DetailView.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 20/12/20.
//

import UIKit

class DetailView: UIView {
    
    @IBOutlet weak var nameEnterprise: UILabel!
    
    @IBOutlet weak var enterpriseImage: UIImageView!
    
    @IBOutlet weak var DescriptionEnterprise: UITextView!
    
}
