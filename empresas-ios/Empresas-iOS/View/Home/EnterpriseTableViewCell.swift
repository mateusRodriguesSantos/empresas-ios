//
//  TableViewCell.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 20/12/20.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var nomeEmpresa: UILabel!
    
    var enterprise:Enterprise?
    
    let net = Network()
    
    override func didMoveToWindow() {
        setConstraints()
        
//        guard let stringPhoto = enterprise?.photo else { return  }
//        
//        let urlString = "\(StringRequests.image)\(stringPhoto)"
//        guard let URL = URL(string: urlString) else { return}
//        backImage.kf.setImage(with: URL)
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        net.allObservers.append(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setConstraints(){
        backImage.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            backImage.heightAnchor.constraint(equalToConstant: 200),
            backImage.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.3),
            backImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10),
            backImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 10),
            backImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
            backImage.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10),
        ])
        
        nomeEmpresa.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            nomeEmpresa.widthAnchor.constraint(equalToConstant: 200),
            nomeEmpresa.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            nomeEmpresa.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            nomeEmpresa.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            nomeEmpresa.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
    }
}

extension EnterpriseTableViewCell:ObserverRequest{
    func notifyEndRequest(_ array: [Enterprise],_ statusLogin: Status) {

    }
}
