//
//  HomeView.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import UIKit

protocol homeViewDelegate: class {
    func successSearch(_ array:[Enterprise])
    func errorSearch()
    func activeSpinner()
    func conectionFail()
}


class HomeView: UIView {
    
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: homeViewDelegate?
    
    let net = Network()
    
    var topSearchConstraint:NSLayoutConstraint?
    var bottomSearchConstraint:NSLayoutConstraint?
    
    let searchBar:UISearchBar = {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.placeholder = "Procure por empresa"
        searchBar.searchBarStyle = .default
        searchBar.layer.zPosition = 1
        searchBar.showsSearchResultsButton = false
        return searchBar
    }()
    
    override func didMoveToWindow() {
        net.allObservers.append(self)
        net.peformRequestSearch(accessToken:  UserDefaults.standard.value(forKey: "accessToken") as! String, client: UserDefaults.standard.value(forKey: "client") as! String, uid:  UserDefaults.standard.value(forKey: "uid") as! String)
        self.addSubview(searchBar)
        setConstraints()
        delegate?.activeSpinner()
    }
    
}

extension HomeView:ObserverRequest{
    func notifyEndRequest(_ array: [Enterprise],_ statusRequest: Status) {
        net.allObservers.removeAll()
        if statusRequest == .success{
            delegate?.successSearch(array)
        }else if statusRequest == .error{
            delegate?.errorSearch()
        }else if statusRequest == .conectionFail{
            delegate?.conectionFail()
        }
    }
    
    func setConstraints(){
        
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        
        topSearchConstraint = searchBar.topAnchor.constraint(equalTo: self.layoutMarginsGuide.topAnchor,constant: 120)
        
        NSLayoutConstraint.activate([
            searchBar.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            searchBar.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            searchBar.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            topSearchConstraint!,
        ])
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.layer.zPosition = 1
        
        bottomSearchConstraint = tableView.topAnchor.constraint(equalTo: self.searchBar.bottomAnchor)
        
        NSLayoutConstraint.activate([
            tableView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height * 0.7),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            bottomSearchConstraint!,
        ])
    }
}
