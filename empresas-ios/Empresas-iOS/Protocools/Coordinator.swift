//
//  Coordinator.swift
//  Empresas-iOS
//
//  Created by Mateus Rodrigues on 19/12/20.
//

import UIKit

protocol Coordinator {
    ///The navigation controller
    var navigationController: UINavigationController { get set }
    
    /**
     This method start the main view
     - Authors: Mateus R.
     - Returns: nothing
     - Parameters: nothing
     */
    func start()
}
